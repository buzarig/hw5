"use strict";

function createNewUser() {
  return {
    _firstName: prompt("What's your name?"),
    _lastName: prompt("What's your last name?"),
    getLogin() {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },

    setFirstName(value) {
      this._firstName = value;
    },

    setLastName(value) {
      this._lastName = value;
    },
  };
}
const newUser = createNewUser();

console.log(newUser.getLogin());
console.log(newUser);
